Clone this repo to get started with a basic LaTeX and MATLAB project setup with appropriate gitignore and gitattribute files. Pay close attention to the PDF ignores.

See [this link](http://stackoverflow.com/questions/6188780/git-latex-workflow) for best practices using git with LaTeX documents.